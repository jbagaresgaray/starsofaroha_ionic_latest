# README

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?

- Quick summary
- Version
- [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up?

- Summary of set up
- Configuration
- Dependencies
- Database configuration
- How to run tests
- Deployment instructions

### Contribution guidelines

- Writing tests
- Code review
- Other guidelines

### Who do I talk to?

- Repo owner or admin
- Other community or team contact

## Fix issue during platform addition

### During fresh installation

ionic cordova platform add ios
ionic cordova platform add android

ionic cordova plugin add cordova-plugin-geofence --force


### During re-installation

ionic cordova plugin rm cordova-plugin-geofence
ionic cordova plugin rm cordova-plugin-add-swift-support

ionic cordova platform rm ios
ionic cordova platform rm android

ionic cordova platform add ios
ionic cordova platform add android

ionic cordova plugin add cordova-plugin-add-swift-support
ionic cordova plugin add cordova-plugin-geofence --force


"cordova-plugin-geofence": {
    "GEOFENCE_ALWAYS_USAGE_DESCRIPTION": "${EXECUTABLE_NAME} Would Like to Use Your Current Location Even In Background.",
    "GEOFENCE_IN_USE_USAGE_DESCRIPTION": "${EXECUTABLE_NAME} Would Like to Use Your Current Location When In Use."
}
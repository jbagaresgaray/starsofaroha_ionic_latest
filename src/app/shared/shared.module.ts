import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

const importExportModules = [
  CommonModule,
  IonicModule,
  FormsModule,
  HttpClientModule,
];

@NgModule({
  declarations: [],
  imports: [...importExportModules],
  exports: [...importExportModules],
})
export class SharedModule {}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'about',
    loadChildren: () =>
      import('./pages/about/about.module').then(m => m.AboutPageModule),
    data: {
      name: 'AboutPage',
    },
  },
  {
    path: 'defination',
    loadChildren: () =>
      import('./pages/defination/defination.module').then(
        m => m.DefinationPageModule
      ),
    data: {
      name: 'DefinationPage',
    },
  },
  {
    path: 'dice-test',
    loadChildren: () =>
      import('./pages/dice-test/dice-test.module').then(
        m => m.DiceTestPageModule
      ),
    data: {
      name: 'DiceTestPage',
    },
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then(m => m.HomePageModule),
    data: {
      name: 'HomePage',
    },
  },
  {
    path: 'how',
    loadChildren: () =>
      import('./pages/how/how.module').then(m => m.HowPageModule),
    data: {
      name: 'HowPage',
    },
  },
  {
    path: 'list',
    loadChildren: () =>
      import('./pages/list/list.module').then(m => m.ListPageModule),
    data: {
      name: 'ListPage',
    },
  },
  {
    path: 'luna',
    loadChildren: () =>
      import('./pages/luna/luna.module').then(m => m.LunaPageModule),
  },
  {
    path: 'mappage',
    loadChildren: () =>
      import('./pages/mappage/mappage.module').then(m => m.MappagePageModule),
    data: {
      name: 'MappagePage',
    },
  },
  {
    path: 'meditation',
    loadChildren: () =>
      import('./pages/meditation/meditation.module').then(
        m => m.MeditationPageModule
      ),
    data: {
      name: 'MeditationPage',
    },
  },
  {
    path: 'metaphor',
    loadChildren: () =>
      import('./pages/metaphor/metaphor.module').then(
        m => m.MetaphorPageModule
      ),
    data: {
      name: 'MetaphorPage',
    },
  },
  {
    path: 'pronunciation',
    loadChildren: () =>
      import('./pages/pronunciation/pronunciation.module').then(
        m => m.PronunciationPageModule
      ),
    data: {
      name: 'PronunciationPage',
    },
  },
  {
    path: 'sliderpage',
    loadChildren: () =>
      import('./pages/sliderpage/sliderpage.module').then(
        m => m.SliderpagePageModule
      ),
    data: {
      name: 'SliderpagePage',
    },
  },
  {
    path: 'soundtrack',
    loadChildren: () =>
      import('./pages/soundtrack/soundtrack.module').then(
        m => m.SoundtrackPageModule
      ),
    data: {
      name: 'SoundtrackPage',
    },
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}

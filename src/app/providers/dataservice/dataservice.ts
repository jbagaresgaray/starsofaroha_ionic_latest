import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MediaObject } from '@ionic-native/media/ngx';

/*
  Generated class for the DataserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable({
  providedIn: 'root',
})
export class DataserviceProvider {
  activePage: any;
  backgroundMusic: MediaObject;
  backgroundIsRelease = false;

  constructor(public http: HttpClient) {}

  getAllPoints() {
    return new Promise((resolve, reject) => {
      this.http.get('./assets/data/points.json').subscribe(
        (resp: any) => {
          resolve(resp);
        },
        error => {
          console.log('error: ', error);
          reject(error);
        }
      );
    });
  }
}

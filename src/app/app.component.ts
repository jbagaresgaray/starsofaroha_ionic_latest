import {
  Component,
  NgZone,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import {
  Platform,
  LoadingController,
  AlertController,
  NavController,
} from '@ionic/angular';
import * as _ from 'lodash';
import generateUUID from 'uuid';

import { constant } from './constants/constant';
import { EventsService } from './providers/events.service';
import { DataserviceProvider } from './providers/dataservice/dataservice';


import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Geofence } from '@ionic-native/geofence/ngx';
import {
  FileTransfer,
  FileTransferObject,
} from '@ionic-native/file-transfer/ngx';
import { Zip } from '@ionic-native/zip/ngx';
import { File } from '@ionic-native/file/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import {
  DeviceMotion,
  DeviceMotionAccelerationData,
} from '@ionic-native/device-motion/ngx';



@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  pages: Array<{ title: string; path: any }>;
  subscription: any;
  previousAcceleration: any = {
    x: null,
    y: null,
    z: null,
  };
  shakeStarted = false;
  moveCounter = 0;
  mediapath: string = constant.mediapath;
  storageDirectory = '';

  androidMediaFolder: string;
  iosMediaFolder: string;
  appVersionData: any = {};
  activePath = '';

  isDownloading: boolean;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public events: EventsService,
    private localNotifications: LocalNotifications,
    public service: DataserviceProvider,
    private geofence: Geofence,
    private deviceMotion: DeviceMotion,
    public zone: NgZone,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private zip: Zip,
    private file: File,
    private appVersion: AppVersion,
    private navCtrl: NavController,
    private transfer: FileTransfer
  ) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.activePath = event.url;
    });

    this.isDownloading = false;

    this.pages = [
      { title: 'Home', path: '/home' },
      { title: 'How to use this app', path: '/how' },
      { title: 'About', path: '/about' },
      { title: 'Māori Pronunciation', path: '/pronunciation' },
      { title: 'Soundtracks', path: '/soundtrack' },
      { title: 'Interactive Map', path: '/mappage' },
      // tslint:disable-next-line:quotemark
      { title: "Luna's Lullaby", path: '/luna' },
    ];

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      const ctrl = this;

      if (this.platform.is('ios')) {
        this.storageDirectory = this.file.documentsDirectory;
        this.iosMediaFolder =
          this.file.documentsDirectory + constant.mediaFolder;
      } else if (this.platform.is('android')) {
        this.storageDirectory =
          this.file.dataDirectory + constant.downloadFolder + '/';
        this.androidMediaFolder =
          this.file.dataDirectory + constant.mediaFolder;
      }

      if (this.platform.is('cordova')) {
        this.file
          .checkFile(ctrl.storageDirectory, constant.packageName)
          .then(result => {
            if (this.platform.is('ios')) {
              ctrl.file
                .checkDir(this.file.documentsDirectory, constant.mediaFolder)
                .then(resp => {})
                .catch(err => {
                  this.extractMediaFile(this.iosMediaFolder);
                });
            } else if (this.platform.is('android')) {
              ctrl.file
                .checkDir(this.file.dataDirectory, constant.mediaFolder)
                .then(resp => {})
                .catch(err => {
                  this.extractMediaFile(this.androidMediaFolder);
                });
            }
          })
          .catch(err => {
            this.downloadMediaFile();
          });

        const options = {
          frequency: 1000,
        };

        this.subscription = this.deviceMotion
          .watchAcceleration(options)
          .subscribe((acceleration: DeviceMotionAccelerationData) => {
            const accelerationChange: any = {};
            if (this.previousAcceleration.x !== null) {
              accelerationChange.x = Math.abs(
                this.previousAcceleration.x - acceleration.x
              );
              accelerationChange.y = Math.abs(
                this.previousAcceleration.y - acceleration.y
              );
              accelerationChange.z = Math.abs(
                this.previousAcceleration.z - acceleration.z
              );
            }

            this.previousAcceleration = {
              x: acceleration.x,
              y: acceleration.y,
              z: acceleration.z,
            };

            if (
              accelerationChange.x +
                accelerationChange.y +
                accelerationChange.z >
              10
            ) {
              this.shakeStarted = true;
              this.moveCounter++;
              console.log('this.shakeStarted 1: ', this.shakeStarted);
            } else if (
              accelerationChange.x +
                accelerationChange.y +
                accelerationChange.z <
                20 &&
              this.shakeStarted === true
            ) {
              this.shakeStarted = false;
              console.log('this.isDownloading: ', this.isDownloading);
              console.log('this.moveCounter: ', this.moveCounter);
              console.log('this.service.activePage: ', this.service.activePage);

              if (this.moveCounter > 1) {
                if (this.service.activePage === 'HomePage') {
                  if (this.isDownloading) {
                    return;
                  }

                  this.events.publish('callToRoll');
                } else if (this.service.activePage === 'SliderpagePage') {
                  if (this.isDownloading) {
                    return;
                  }
                  this.events.publish('shakeSliderStop');
                }
                this.moveCounter = 0;
              }
            }
          });

        this.appVersion.getAppName().then((appName: string) => {
          this.appVersionData.appName = appName;
        });
        this.appVersion.getPackageName().then((packageName: string) => {
          this.appVersionData.packageName = packageName;
        });
        this.appVersion.getVersionCode().then((versionCode: string) => {
          this.appVersionData.versionCode = versionCode;
        });
        this.appVersion.getVersionNumber().then((versionNumber: string) => {
          this.appVersionData.versionNumber = versionNumber;
        });
      }
    });
  }

  async extractMediaFile(extDir) {
    const newDir = this.storageDirectory + constant.packageName;
    // const extDir = this.file.externalDataDirectory + constant.mediaFolder;
    const loading = await this.loadingCtrl.create({
      message: 'Extracting Media...',
    });
    loading.present();
    this.zip
      .unzip(newDir, extDir, progress => {
        const progressCount = (progress.loaded / progress.total) * 100;
        const perc = Math.round(progressCount) + '%';
        this.zone.run(() => {
          loading.message = 'Extracting Media ' + perc + '...';
          console.log('Extracting Media ' + progressCount + '%...');
        });
      })
      .then(result => {
        loading.dismiss();

        this.isDownloading = false;

        if (result === 0) {
          console.log('SUCCESS');
        }
        if (result === -1) {
          console.log('FAILED');
        }
      });
  }

  async deleteFile() {
    const existingDir = this.storageDirectory + constant.packageName;
    const loading = await this.loadingCtrl.create({
      message: 'Clearing cache...',
    });

    const ExecuteThis = () => {
      if (this.platform.is('cordova')) {
        if (this.platform.is('ios')) {
          this.file
            .removeRecursively(
              this.file.documentsDirectory,
              constant.mediaFolder
            )
            .then(
              (data: any) => {
                console.log('removeRecursively: ', data);
                if (data.success) {
                  this.reDownloadFile();
                }
              },
              error => {
                console.log('removeRecursively error: ', error);
                this.reDownloadFile();
              }
            );
        } else if (this.platform.is('android')) {
          this.file
            .removeRecursively(this.file.dataDirectory, constant.mediaFolder)
            .then(
              (data: any) => {
                console.log('removeRecursively: ', data);
                if (data.success) {
                  this.reDownloadFile();
                }
              },
              error => {
                console.log('removeRecursively error: ', error);
                this.reDownloadFile();
              }
            );
        }
      }
    };

    const RedownloadThis = () => {
      if (this.platform.is('cordova')) {
        loading.present();
        this.file.removeFile(this.storageDirectory, constant.packageName).then(
          (data: any) => {
            console.log('removeFile: ', data);
            loading.dismiss();
            if (this.platform.is('cordova') && data.success) {
              ExecuteThis();
            }
          },
          error => {
            console.log(' removeFileerror: ', error);
            loading.dismiss();
            if (this.platform.is('cordova')) {
              ExecuteThis();
            }
          }
        );
      }
    };

    const confirm = await this.alertCtrl.create({
      header: 'Refresh Cache?',
      message:
        'This will redownload or refresh downloaded media files. Continue?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          },
        },
        {
          text: 'Yes',
          handler: () => {
            RedownloadThis();
          },
        },
      ],
    });
    confirm.present();
  }

  reDownloadFile() {
    if (this.platform.is('cordova')) {
      // CHECK FILE before downloading
      this.file
        .checkFile(this.storageDirectory, constant.packageName)
        .then(result => {
          if (this.platform.is('ios')) {
            this.file
              .checkDir(this.file.documentsDirectory, constant.mediaFolder)
              .then(resp => {})
              .catch(err => {
                this.extractMediaFile(this.iosMediaFolder);
              });
          } else if (this.platform.is('android')) {
            this.file
              .checkDir(this.file.dataDirectory, constant.mediaFolder)
              .then(resp => {})
              .catch(err => {
                this.extractMediaFile(this.androidMediaFolder);
              });
          }
        })
        .catch(err => {
          this.downloadMediaFile();
        });
    }
  }

  async downloadMediaFile() {
    const loading = await this.loadingCtrl.create({
      message: 'Downloading Media...',
    });
    loading.present();

    this.isDownloading = true;

    const newDir = this.storageDirectory + constant.packageName;
    const url = this.mediapath + constant.packageName;
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(url, newDir).then(
      entry => {
        if (entry) {
          loading.dismiss();

          if (this.platform.is('ios')) {
            this.extractMediaFile(this.iosMediaFolder);
          } else if (this.platform.is('android')) {
            this.extractMediaFile(this.androidMediaFolder);
          }
        }
      },
      error => {
        console.log('error: ', error);
        this.isDownloading = false;
        this.alertCtrl
          .create({
            subHeader: `Download Failed!`,
            message: `File was not successfully downloaded. Error code: ${error.code}`,
            buttons: ['Ok'],
          })
          .then(alert => alert.present());
        loading.dismiss();
      }
    );

    fileTransfer.onProgress(progressEvent => {
      if (progressEvent.lengthComputable) {
        const progressCount =
          (progressEvent.loaded / progressEvent.total) * 100;
        const perc = Math.floor(progressCount);
        this.zone.run(() => {
          loading.message = 'Downloading Media ' + perc + '% ...';
          console.log('Downloading Media ' + progressCount + '% ...');
        });
      }
    });
  }

  setupGeoFence() {
    const geoFencesArr: any[] = [];

    function htmlToPlaintext(text) {
      return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    }

    this.service
      .getAllPoints()
      .then((data: any) => {
        if (!_.isEmpty(data)) {
          const pointsArr: any[] = data.points;
          _.each(pointsArr, (row: any) => {
            let i = 1;
            const title = htmlToPlaintext(row.name);
            geoFencesArr.push({
              id: generateUUID(),
              latitude: parseFloat(row.location.lat),
              longitude: parseFloat(row.location.lng),
              radius: Math.ceil(row.location.radius),
              transitionType: 1,
              notification: {
                id: parseInt(row.id, 0),
                title,
                text: row.shortDesc,
                openAppOnClick: true,
              },
            });
            i++;
          });
        }
      })
      .then(() => {
        this.geofence.initialize().then(
          () => {
            this.geofence.addOrUpdate(geoFencesArr).then(
              () => console.log('Geofence added'),
              err => console.log('Geofence failed to add: ', err)
            );

            this.geofence.onTransitionReceived().subscribe(data => {
              console.log('onTransitionReceived: ', data);
              // this.events.publish('transition:recieved');
            });

            this.geofence.onNotificationClicked().subscribe(data => {
              console.log('onNotificationClicked: ', data);
              if (data) {
                this.navCtrl.navigateForward('/sliderpage', {
                  queryParams: {
                    id: data.id,
                    slideTo: 0,
                  },
                });
              }
            });
          },
          err => console.log(err)
        );
      });
  }

  openPage(page) {
    if (page.title === 'Test Notification') {
      console.log('Notification');
      this.localNotifications.schedule({
        id: 1,
        text: 'Sample Notification',
        sound: 'file://assets/media/ding.mp3',
      });
    } else {
      this.navCtrl.navigateRoot(page.path);
    }
  }
}

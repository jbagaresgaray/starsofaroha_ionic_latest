import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';

import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { DeviceMotion } from '@ionic-native/device-motion/ngx';
import { Media } from '@ionic-native/media/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { GoogleMaps } from '@ionic-native/google-maps/ngx';
import { Geofence } from '@ionic-native/geofence/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { Zip } from '@ionic-native/zip/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
      backButtonText: '',
      backButtonIcon: 'chevron-back-outline',
      swipeBackEnabled: false,
    }),
    AppRoutingModule,
    SharedModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    StatusBar,
    DeviceMotion,
    Media,
    LocalNotifications,
    GoogleMaps,
    Geofence,
    File,
    FilePath,
    Zip,
    AppVersion,
    NativeAudio,
    WebView,
    FileTransfer,
    LaunchNavigator,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

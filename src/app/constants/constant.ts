export const constant = {
  mediapath: 'https://d2zgu0xkvvrun9.cloudfront.net/media/',
  packageName: 'soa-package-v2.zip',
  packageFolder: 'soa-package-v2',
  mediaFolder: 'media',
  downloadFolder: 'Download',
};

import { Component, ViewEncapsulation } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import * as _ from 'lodash';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

@Component({
  selector: 'page-metaphor',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'metaphor.html',
})
export class MetaphorPage {
  selectedItem: any;
  pointsArr: any[] = [];
  currentPoint: any = {};
  point: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: DataserviceProvider
  ) {
    this.selectedItem = navParams.get('id');
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter MetaphorPage');
    this.service.getAllPoints().then((data: any) => {
      if (!_.isEmpty(data)) {
        this.pointsArr = data.points;
        this.currentPoint = _.find(this.pointsArr, {
          id: this.selectedItem.toString(),
        });
        this.point = {
          maoriName: this.currentPoint.maoriName,
          metaphor: this.currentPoint.metaphor,
          image_url: './assets/img/' + this.selectedItem + '.jpg',
          icon_url: './assets/imgs/' + this.selectedItem + '.png',
        };
      }
    });
  }

  swipeEvent(ev) {
    console.log('swipeEvent: ', ev.direction);
    if (ev.direction === 4) {
      // RIGHT
      // this.navCtrl.popTo(DefinationPage, { id: this.selectedItem });
      this.navCtrl.navigateBack('/definition', {
        queryParams: { id: this.selectedItem },
      });
    } else if (ev.direction === 2) {
      // LEFT
      // this.navCtrl.push(MeditationPage, { id: this.selectedItem });
      this.navCtrl.navigateForward('/meditation', {
        queryParams: { id: this.selectedItem },
      });
    }
  }
}

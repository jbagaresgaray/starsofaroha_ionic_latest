import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { MetaphorPage } from './metaphor';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: MetaphorPage,
      },
    ]),
  ],
  declarations: [MetaphorPage],
})
export class MetaphorPageModule {}

import { Component, ViewEncapsulation } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'page-pronunciation',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'pronunciation.html',
  styleUrls: ['pronunciation.scss'],
})
export class PronunciationPage {
  constructor(private menuCtrl: MenuController) {}

  ionViewDidEnter() {
    console.log('ionViewDidEnter PronunciationPage');
  }

  menutToggle() {
    this.menuCtrl.enable(true, 'main-menu');
    this.menuCtrl.toggle('main-menu');
  }
}

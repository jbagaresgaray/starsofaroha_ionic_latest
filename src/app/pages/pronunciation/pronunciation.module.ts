import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { PronunciationPage } from './pronunciation';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PronunciationPage,
      },
    ]),
  ],
  declarations: [PronunciationPage],
})
export class PronunciationPageModule {}

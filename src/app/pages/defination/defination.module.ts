import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { DefinationPage } from './defination';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: DefinationPage,
      },
    ]),
  ],
  declarations: [DefinationPage],
})
export class DefinationPageModule {}

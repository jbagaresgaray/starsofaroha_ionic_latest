import { Component, ViewEncapsulation } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import * as _ from 'lodash';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

@Component({
  selector: 'page-defination',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'defination.html',
  styleUrls: ['defination.scss'],
})
export class DefinationPage {
  selectedItem: any;
  pointsArr: any[] = [];
  currentPoint: any = {};
  point: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: DataserviceProvider
  ) {
    this.selectedItem = navParams.get('id');
  }

  ionViewDidEnter() {
    console.log('DefinationPage');
    this.service.getAllPoints().then((data: any) => {
      if (!_.isEmpty(data)) {
        this.pointsArr = data.points;
        this.currentPoint = _.find(this.pointsArr, {
          id: this.selectedItem.toString(),
        });
        this.point = {
          maoriName: this.currentPoint.maoriName,
          defination: this.currentPoint.defination,
          image_url: './assets/img/' + this.selectedItem + '.jpg',
          icon_url: './assets/imgs/' + this.selectedItem + '.png',
        };
      }
    });
  }

  swipeEvent(ev) {
    console.log('swipeEvent: ', ev.direction);
    if (ev.direction === 4) {
      // RIGHT
      // this.navCtrl.popTo(ListPage, { id: this.selectedItem });
      this.navCtrl.navigateBack('/list', {
        queryParams: {
          id: this.selectedItem,
        },
      });
    } else if (ev.direction === 2) {
      // LEFT
      // this.navCtrl.push(MetaphorPage, { id: this.selectedItem });
      this.navCtrl.navigateForward('/metaphor', {
        queryParams: {
          id: this.selectedItem,
        },
      });
    }
  }
}

import { Component, ViewEncapsulation } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'page-about',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'about.html',
  styleUrls: ['about.scss'],
})
export class AboutPage {
  constructor(private menutCtrl: MenuController) {}

  ionViewDidEnter() {
    console.log('ionViewDidEnter AboutPage');
  }

  menutToggle() {
    this.menutCtrl.enable(true, 'main-menu');
    this.menutCtrl.toggle('main-menu');
  }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { AboutPage } from './about';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: AboutPage,
      },
    ]),
  ],
  declarations: [AboutPage],
})
export class AboutPageModule {}

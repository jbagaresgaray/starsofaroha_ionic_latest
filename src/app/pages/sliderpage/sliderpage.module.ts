import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

import { SliderpagePage } from './sliderpage';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: SliderpagePage,
      },
    ]),
  ],
  declarations: [SliderpagePage],
  exports: [SliderpagePage],
  entryComponents: [SliderpagePage],
})
export class SliderpagePageModule {}

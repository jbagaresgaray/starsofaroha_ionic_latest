import { Component, ViewChild, NgZone, ViewEncapsulation } from '@angular/core';
import {
  NavController,
  NavParams,
  IonSlides,
  Platform,
  PopoverController,
  IonFab,
  ActionSheetController,
} from '@ionic/angular';

import * as _ from 'lodash';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

import { Media, MediaObject, MEDIA_STATUS } from '@ionic-native/media/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { File, FileEntry, DirectoryEntry } from '@ionic-native/file/ngx';

import { constant } from '../../constants/constant';
import { EventsService } from '../../providers/events.service';
import {
  Router,
  RouterEvent,
  NavigationEnd,
  ActivatedRoute,
} from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'page-sliderpage',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'sliderpage.html',
  styleUrls: ['sliderpage.scss'],
})
export class SliderpagePage {
  @ViewChild(IonSlides, { static: true }) slides: IonSlides;
  selectedItem: any;

  pointsArr: any[] = [];
  currentPoint: any = {};
  point: any = {};
  metapoint: any = {};
  medipoint: any = {};
  showFooter: boolean;
  playing: boolean;
  stop: boolean;

  currentIndex: number;
  slideTo: number;

  apptitle: string;

  view: any;
  soundtrack: any = 'reading';

  file1: MediaObject;
  file2: MediaObject;
  file3: MediaObject;
  storageDirectory = '';
  musicStorageDirectory = '';

  @ViewChild('fab', { static: true }) fab: IonFab;
  unsubscribe$ = new Subject<any>();
  backgroundImage: any = {};

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public events: EventsService,
    public popoverCtrl: PopoverController,
    private actionSheetCtrl: ActionSheetController,
    public zone: NgZone,
    public service: DataserviceProvider,
    private nativeAudio: NativeAudio,
    private file: File,
    private media: Media,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.playing = false;
    this.stop = false;
    this.showFooter = false;

    this.router.events
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          if (this.activatedRoute.root) {
            const firstChild = this.activatedRoute.root.firstChild;

            firstChild.data
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe((params: any) => {
                console.log('params: ', params);
                if (params && params.name) {
                  this.view = params;
                  this.service.activePage = params.name;
                }
              });

            firstChild.queryParams
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe((params: any) => {
                console.log('queryParams: ', params);
                if (params && params.id) {
                  this.selectedItem = params.id;
                } else if (params && params.slideTo) {
                  this.slideTo = params.slideTo;
                }
              });
          }
        }
      });

    this.events.subscribe('shakeSliderStop', () => {
      this.shakeSliderStop();
    });
  }

  shakeSliderStop() {
    if (this.currentIndex >= 3 && this.slides.isEnd()) {
      this.navCtrl.navigateRoot('/home', {
        queryParams: { navigate: 'navigate' },
        animated: true,
        animationDirection: 'back',
      });
    }
  }

  ionViewWillEnter() {
    this.service.activePage = this.view.name;

    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        if (this.platform.is('ios')) {
          this.storageDirectory =
            this.file.documentsDirectory +
            constant.mediaFolder +
            '/' +
            constant.packageFolder +
            '/';

          /*this.musicStorageDirectory = this.file.documentsDirectory + 'media/';
				  this.musicStorageDirectory = this.webview.convertFileSrc(this.musicStorageDirectory);*/
          this.musicStorageDirectory = './assets/' + constant.mediaFolder + '/';

          this.file
            .resolveLocalFilesystemUrl(this.storageDirectory)
            .then((entry: FileEntry) => {
              const newPath = entry.toInternalURL();

              this.createMediaFile1(newPath + this.selectedItem + '-1.mp3');
              this.createMediaFile2(newPath + this.selectedItem + '-2.mp3');
              this.createMediaFile3(newPath + this.selectedItem + '-3.mp3');
            });
        } else if (this.platform.is('android')) {
          this.storageDirectory =
            this.file.dataDirectory +
            constant.mediaFolder +
            '/' +
            constant.packageFolder +
            '/';

          this.musicStorageDirectory =
            this.file.dataDirectory +
            constant.mediaFolder +
            '/' +
            constant.packageFolder +
            '/';

          this.createMediaFile1(
            this.storageDirectory + this.selectedItem + '-1.mp3'
          );
          this.createMediaFile2(
            this.storageDirectory + this.selectedItem + '-2.mp3'
          );
          this.createMediaFile3(
            this.storageDirectory + this.selectedItem + '-3.mp3'
          );
        }
      }
    });

    this.forViewDidEnter();
  }

  ionViewWillLeave() {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        if (this.file1) {
          this.file1.stop();

          if (this.platform.is('android')) {
            this.file1.release();
          }
        }

        if (this.file2) {
          this.file2.stop();

          if (this.platform.is('android')) {
            this.file2.release();
          }
        }

        if (this.file3) {
          this.file3.stop();

          if (this.platform.is('android')) {
            this.file3.release();
          }
        }

        this.playing = false;
        // this.stop = true;

        if (this.platform.is('android')) {
          if (this.service.backgroundMusic) {
            this.service.backgroundMusic.stop();
            this.service.backgroundMusic.release();

            this.service.backgroundIsRelease = true;
          }
        } else if (this.platform.is('ios')) {
          this.nativeAudio.stop('uniqueId2').then(() => {
            this.nativeAudio.unload('uniqueId2');
          });
        }
      }
    });

    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  headerClass(selectedItem) {
    switch (parseInt(selectedItem, 0)) {
      case 1:
        return 'one';
      case 2:
        return 'two';
      case 3:
        return 'three';
      case 4:
        return 'four';
      case 5:
        return 'five';
      case 6:
        return 'six';
      case 7:
        return 'seven';
      case 8:
        return 'eight';
      case 9:
        return 'nine';
      case 10:
        return 'ten';
      case 11:
        return 'eleven';
      case 12:
        return 'twelve';
      case 13:
        return 'thirteen';
      case 14:
        return 'fourteen';
      case 15:
        return 'fifteen';
      case 16:
        return 'sixteen';
      case 17:
        return 'seventeen';
      case 18:
        return 'eighteen';
      case 19:
        return 'nineteen';
      case 20:
        return 'twenty';
    }
  }

  private async forViewDidEnter() {
    const slideNum = await this.slides.length();
    if (this.slides && slideNum > 0) {
      if (this.slideTo) {
        this.slides.slideTo(this.slideTo);
      }
    }

    const htmlToPlaintext = (text: string) => {
      return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };

    const data: any = await this.service.getAllPoints();
    if (!_.isEmpty(data)) {
      this.pointsArr = data.points;
      this.currentPoint = _.find(this.pointsArr, {
        id: this.selectedItem.toString(),
      });

      this.zone.run(() => {
        this.point = {
          title: this.currentPoint.name,
          shortDescription: this.currentPoint.shortDesc,
          maoriName: this.currentPoint.maoriName,
          defination: this.currentPoint.defination,
          metaphor: this.currentPoint.metaphor,
          meditation: this.currentPoint.meditation,
          image_url: './assets/img/' + this.selectedItem + '.jpg',
          icon_url: './assets/imgs/' + this.selectedItem + '.png',
        };

        this.backgroundImage = {
          '--background': 'url(' + this.point.image_url + ')',
          background: 'url(' + this.point.image_url + ')',
        };

        this.apptitle = htmlToPlaintext(this.currentPoint.maoriName);
      });
    }
  }

  private createMediaFile1(newPath) {
    this.file1 = this.media.create(newPath);
    console.log('this.File1: ', this.file1);

    this.file1.onStatusUpdate.subscribe(status => {
      console.log('File1 Status: ', status);
    });

    this.file1.onSuccess.subscribe(() => {
      console.log('File1 Action is successful');
    });

    this.file1.onError.subscribe(error => {
      console.log('File1 Error!', error);
      console.log('File1 Error!', JSON.stringify(error));
    });
  }

  private createMediaFile2(newPath) {
    this.file2 = this.media.create(newPath);
    console.log('this.File2: ', this.file2);

    this.file2.onStatusUpdate.subscribe(status => {
      console.log('File2 Status: ', status);
    });

    this.file2.onSuccess.subscribe(() => {
      console.log('File2 Action is successful');
    });

    this.file2.onError.subscribe(error => {
      console.log('File2 Error!', error);
      console.log('File2 Error!', JSON.stringify(error));
    });
  }

  private createMediaFile3(newPath) {
    this.file3 = this.media.create(newPath);
    console.log('this.File3: ', this.file3);

    this.file3.onStatusUpdate.subscribe(status => {
      console.log('File3 Status: ', status);
    });

    this.file3.onSuccess.subscribe(() => {
      console.log('File3 Action is successful');
    });

    this.file3.onError.subscribe(error => {
      console.log('File3 Error!', error);
      console.log('File3 Error!', JSON.stringify(error));
    });
  }

  async slideChanged() {
    this.currentIndex = await this.slides.getActiveIndex();
    if (this.fab) {
      this.fab.close();
    }

    if (this.currentIndex > 0) {
      if (this.platform.is('cordova')) {
        if (this.file1) {
          this.file1.stop();
        }

        if (this.file2) {
          this.file2.stop();
        }

        if (this.file3) {
          this.file3.stop();
        }
      }
    }
    if (this.platform.is('ios')) {
      this.storageDirectory =
        this.file.documentsDirectory +
        constant.mediaFolder +
        '/' +
        constant.packageFolder +
        '/';
    } else if (this.platform.is('android')) {
      this.storageDirectory =
        this.file.dataDirectory +
        constant.mediaFolder +
        '/' +
        constant.packageFolder +
        '/';
    }

    if (this.currentIndex === 0 && this.slides.isBeginning()) {
      this.showFooter = false;
      this.zone.run(() => {
        this.showFooter = false;
      });

      this.fabButtonStopAudioEv();
    } else if (this.currentIndex === 1) {
      this.showFooter = true;
      this.zone.run(() => {
        this.showFooter = true;
      });

      if (this.platform.is('cordova')) {
        this.platform.ready().then(() => {
          if (this.platform.is('ios')) {
            this.file
              .resolveLocalFilesystemUrl(this.storageDirectory)
              .then((entry: FileEntry) => {
                const newPath = entry.toInternalURL();
                this.createMediaFile1(newPath + this.selectedItem + '-1.mp3');

                if (this.file1) {
                  this.file1.stop();
                }

                this.playing = false;
                this.stop = true;
              });
          } else if (this.platform.is('android')) {
            this.createMediaFile1(
              this.storageDirectory + this.selectedItem + '-1.mp3'
            );

            if (this.file1) {
              this.file1.stop();
            }

            this.playing = false;
            this.stop = true;
          }
        });
      }
    } else if (this.currentIndex === 2) {
      this.showFooter = true;
      this.zone.run(() => {
        this.showFooter = true;
      });

      if (this.platform.is('cordova')) {
        this.platform.ready().then(() => {
          if (this.platform.is('ios')) {
            this.file
              .resolveLocalFilesystemUrl(this.storageDirectory)
              .then((entry: FileEntry) => {
                const newPath = entry.toInternalURL();
                this.createMediaFile2(newPath + this.selectedItem + '-2.mp3');

                if (this.file2) {
                  this.file2.stop();
                }

                this.playing = false;
                this.stop = true;
              });
          } else if (this.platform.is('android')) {
            this.createMediaFile2(
              this.storageDirectory + this.selectedItem + '-2.mp3'
            );

            if (this.file2) {
              this.file2.stop();
            }

            this.playing = false;
            this.stop = true;
          }
        });
      }
    } else if (this.currentIndex === 3) {
      this.showFooter = true;

      this.zone.run(() => {
        this.showFooter = true;
      });

      if (this.platform.is('cordova')) {
        this.platform.ready().then(() => {
          if (this.platform.is('ios')) {
            this.file
              .resolveLocalFilesystemUrl(this.storageDirectory)
              .then((entry: FileEntry) => {
                const newPath = entry.toInternalURL();
                this.createMediaFile3(newPath + this.selectedItem + '-3.mp3');

                if (this.file3) {
                  this.file3.stop();
                }

                this.playing = false;
                this.stop = true;
              });
          } else if (this.platform.is('android')) {
            this.createMediaFile3(
              this.storageDirectory + this.selectedItem + '-3.mp3'
            );

            if (this.file3) {
              this.file3.stop();
            }

            this.playing = false;
            this.stop = true;
          }
        });
      }
    } else {
      this.showFooter = true;
      this.zone.run(() => {
        this.showFooter = true;
      });
    }
  }

  async fabButtonPlayAudioEv() {
    this.currentIndex = await this.slides.getActiveIndex();
    switch (this.currentIndex) {
      case 1:
        this.platform.ready().then(() => {
          if (this.platform.is('cordova')) {
            if (this.file1) {
              if (this.playing) {
                this.file1.pause();
                this.playing = false;
              } else {
                this.file1.play();
                this.playing = true;
              }
            }
          }
        });
        break;
      case 2:
        this.platform.ready().then(() => {
          if (this.platform.is('cordova')) {
            if (this.file2) {
              if (this.playing) {
                this.file2.pause();
                this.playing = false;
              } else {
                this.file2.play();
                this.playing = true;
              }
            }
          }
        });
        break;
      case 3:
        this.platform.ready().then(() => {
          if (this.platform.is('cordova')) {
            if (this.file3) {
              if (this.playing) {
                this.file3.pause();
                this.playing = false;
              } else {
                this.file3.play();
                this.playing = true;
              }
            }
          }
        });
        break;
      default:
        // code...
        break;
    }
    this.stop = false;
  }

  async fabButtonPauseAudioEv() {
    this.currentIndex = await this.slides.getActiveIndex();
    switch (this.currentIndex) {
      case 1:
        this.platform.ready().then(() => {
          if (this.platform.is('cordova')) {
            if (this.file1) {
              this.file1.pause();
            }
          }
        });
        break;
      case 2:
        this.platform.ready().then(() => {
          if (this.platform.is('cordova')) {
            if (this.file2) {
              this.file2.pause();
            }
          }
        });
        break;
      case 3:
        this.platform.ready().then(() => {
          if (this.platform.is('cordova')) {
            if (this.file3) {
              this.file3.pause();
            }
          }
        });
        break;
      default:
        // code...
        break;
    }

    this.playing = false;
    this.stop = false;
  }

  async fabButtonStopAudioEv() {
    this.currentIndex = await this.slides.getActiveIndex();
    switch (this.currentIndex) {
      case 1:
        this.platform.ready().then(() => {
          if (this.platform.is('cordova')) {
            if (this.file1) {
              this.file1.stop();
            }
          }
        });
        break;
      case 2:
        this.platform.ready().then(() => {
          if (this.platform.is('cordova')) {
            if (this.file2) {
              this.file2.stop();
            }
          }
        });
        break;
      case 3:
        this.platform.ready().then(() => {
          if (this.platform.is('cordova')) {
            if (this.file3) {
              this.file3.stop();
            }
          }
        });
        break;
      default:
        this.platform.ready().then(() => {
          if (this.platform.is('cordova')) {
            if (this.file1) {
              this.file1.stop();
            }

            if (this.file2) {
              this.file2.stop();
            }

            if (this.file3) {
              this.file3.stop();
            }
          }
        });
        break;
    }

    this.playing = false;
    this.stop = true;
  }

  buttonPlayAudioEv(file) {
    if (file === 'reading') {
      if (this.platform.is('cordova')) {
        if (this.platform.is('android')) {
          if (this.service.backgroundMusic) {
            this.service.backgroundMusic.stop();
            this.service.backgroundMusic.release();

            this.service.backgroundIsRelease = true;
            this.actionSheetCtrl.dismiss();
            return false;
          }
        } else if (this.platform.is('ios')) {
          this.nativeAudio.stop('uniqueId2').then(() => {
            this.nativeAudio.unload('uniqueId2');
          });
        }
      }
    } else {
      const playNativeAudio = (path: string) => {
        this.platform.ready().then(() => {
          this.nativeAudio.unload('uniqueId2');
          this.nativeAudio.preloadComplex('uniqueId2', path, 1, 1, 0).then(
            (response: any) => {
              console.log('preloadComplex: ', response);
              this.nativeAudio.play('uniqueId2', () => {
                console.log('uniqueId2 is done playing');
              });
            },
            onError => {
              console.log('nativeAudio onError: ', onError);
              console.log('nativeAudio onError: ', JSON.stringify(onError));
            }
          );
        });
      };

      if (this.platform.is('cordova')) {
        this.platform.ready().then(() => {
          if (this.platform.is('android')) {
            console.log('android');

            if (this.service.backgroundMusic) {
              this.service.backgroundMusic.stop();
              this.service.backgroundMusic.release();
            }

            const path: string = this.musicStorageDirectory + file + '.mp3';
            this.service.backgroundMusic = this.media.create(path);
            this.service.backgroundMusic.play({
              playAudioWhenScreenIsLocked: true,
              numberOfLoops: 999,
            });
            this.service.backgroundMusic.setVolume(0.3);
            this.service.backgroundIsRelease = false;

            if (this.service.backgroundMusic) {
              this.service.backgroundMusic.onStatusUpdate.subscribe(status => {
                // Infinite Loop for Android
                if (this.platform.is('android')) {
                  if (status === MEDIA_STATUS.STOPPED) {
                    if (!this.service.backgroundIsRelease) {
                      this.service.backgroundMusic.seekTo(0);
                      this.service.backgroundMusic.play();
                    }
                  }
                }
              });

              this.service.backgroundMusic.onSuccess.subscribe(() =>
                console.log('Action is successful')
              );
              this.service.backgroundMusic.onError.subscribe((error: any) => {
                console.log('Error!', error);
                console.log('Error Code!', error.code);
                console.log('Error Mesg!', error.message);
              });
            }
          } else if (this.platform.is('ios')) {
            console.log('ios: ', this.musicStorageDirectory);
            const nativeMedia = this.musicStorageDirectory + file + '.mp3';
            console.log('nativeMedia: ', nativeMedia);
            playNativeAudio(nativeMedia);
          }
        });
      }
      this.actionSheetCtrl.dismiss();
    }
  }

  async presentPopover(myEvent: any) {
    const actionSheet = await this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'None',
          role: 'destructive',
          handler: () => {
            this.buttonPlayAudioEv('reading');
          },
        },
        {
          text: 'NZ Birdlife Soundtrack',
          handler: () => {
            this.buttonPlayAudioEv('birdlifeMusic');
          },
        },
        {
          text: 'NZ Forest Soundtrack',
          handler: () => {
            this.buttonPlayAudioEv('forestMusic');
          },
        },
        {
          text: 'NZ Ocean Soundtrack',
          handler: () => {
            this.buttonPlayAudioEv('oceanMusic');
          },
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await actionSheet.present();
  }
}

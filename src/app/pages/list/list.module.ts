import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { ListPage } from './list';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListPage,
      },
    ]),
  ],
  declarations: [ListPage],
})
export class ListPageModule {}

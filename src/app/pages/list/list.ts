import { Component, ViewEncapsulation } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import * as _ from 'lodash';

import { DefinationPage } from '../defination/defination';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-list',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'list.html',
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  pointsArr: any[] = [];
  currentPoint: any = {};
  point: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: DataserviceProvider
  ) {
    this.selectedItem = navParams.get('id');
  }

  ionViewDidEnter() {
    console.log('DetailsPage');
    this.service.getAllPoints().then((data: any) => {
      if (!_.isEmpty(data)) {
        this.pointsArr = data.points;
        this.currentPoint = _.find(this.pointsArr, {
          id: this.selectedItem.toString(),
        });
        this.point = {
          title: this.currentPoint.name,
          maoriName: this.currentPoint.maoriName,
          image_url: './assets/img/' + this.selectedItem + '.jpg',
          shortDescription: this.currentPoint.shortDesc,
        };
      }
    });
  }

  swipeEvent(ev) {
    console.log('swipeEvent: ', ev.direction);
    if (ev.direction === 4) {
      this.navCtrl.navigateBack('/home');
    } else if (ev.direction === 2) {
      this.navCtrl.navigateForward('/defination', {
        queryParams: { id: this.selectedItem },
      });
    }
  }
}

import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { NavController, NavParams, Platform, IonContent } from '@ionic/angular';
import * as _ from 'lodash';
import { Media, MediaObject } from '@ionic-native/media/ngx';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import { EventsService } from 'src/app/providers/events.service';
import {
  Router,
  RouterEvent,
  NavigationEnd,
  ActivatedRoute,
} from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'page-meditation',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'meditation.html',
  // styleUrls: ['meditation.scss'],
})
export class MeditationPage {
  selectedItem: any;
  pointsArr: any[] = [];
  currentPoint: any = {};
  point: any = {};
  soundtrack: any = 'reading';
  file: MediaObject;

  playing: boolean;
  stop: boolean;

  @ViewChild(IonContent, { static: true }) content: IonContent;
  unsubscribe$ = new Subject<any>();
  private view: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: DataserviceProvider,
    private media: Media,
    public events: EventsService
  ) {
    this.selectedItem = navParams.get('id');
    // this.selectedItem = 2;

    this.playing = false;
    this.stop = false;

    this.router.events
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          console.log('this.activatedRoute: ', this.activatedRoute);

          if (this.activatedRoute.root) {
            this.activatedRoute.root.firstChild.data
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe((params: any) => {
                console.log('params: ', params);
                if (params && params.name) {
                  this.view = params;
                  this.service.activePage = params.name;
                }
              });
          }
        }
      });

    this.events.subscribe('shakeStop', () => {
      if (this.view.name === 'MeditationPage') {
        this.navCtrl.navigateBack('/home', {
          queryParams: {
            navigate: 'navigate',
          },
        });
      }
    });
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter MeditationPage');
    this.service.getAllPoints().then((data: any) => {
      if (!_.isEmpty(data)) {
        this.pointsArr = data.points;
        this.currentPoint = _.find(this.pointsArr, {
          id: this.selectedItem.toString(),
        });
        this.point = {
          maoriName: this.currentPoint.maoriName,
          meditation: this.currentPoint.meditation,
          image_url: './assets/img/' + this.selectedItem + '.jpg',
          icon_url: './assets/imgs/' + this.selectedItem + '.png',
        };
      }
    });

    this.platform.ready().then(() => {
      let path: string;
      /*if (this.platform.is('android')) {
        path = 'file:///android_asset/www/assets/media/' + this.selectedItem + '.mp3';
      } else {
        path = './assets/media/' + this.selectedItem + '.mp3'
      }*/
      path =
        'https://d2zgu0xkvvrun9.cloudfront.net/media/' +
        this.selectedItem +
        '.mp3';
      this.file = this.media.create(path);
      console.log('this.file: ', this.file);
    });
  }

  ionViewWillLeave() {
    this.events.unsubscribe('shakeStop');
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  buttonPlayAudioEv() {
    if (this.platform.is('cordova')) {
      this.file.play({ numberOfLoops: 1 });
    }
    this.playing = true;
    this.stop = false;
  }

  buttonPauseAudioEv() {
    if (this.platform.is('cordova')) {
      if (this.file) {
        this.file.pause();
      }
    }
    this.playing = false;
    this.stop = false;
  }

  buttonStopAudioEv() {
    if (this.platform.is('cordova')) {
      if (this.file) {
        this.file.stop();
      }
    }

    this.playing = false;
    this.stop = true;
  }

  _change() {
    console.log('_change: ', this.soundtrack);
    this.buttonStopAudioEv();
    let path: string;

    if (this.soundtrack === 'reading') {
      /*if (this.platform.is('android')) {
        path = 'file:///android_asset/www/assets/media/' + this.selectedItem + '.mp3';
      } else {
        path = './assets/media/' + this.selectedItem + '.mp3'
      }*/
      path =
        'https://d2zgu0xkvvrun9.cloudfront.net/media/' +
        this.selectedItem +
        '.mp3';
      this.file = this.media.create(path);
    } else {
      /*if (this.platform.is('android')) {
        path = 'file:///android_asset/www/assets/media/' + this.soundtrack + '.mp3';
      } else {
        path = './assets/media/' + this.soundtrack + '.mp3'
      }*/
      path =
        'https://d2zgu0xkvvrun9.cloudfront.net/media/' +
        this.soundtrack +
        '.mp3';
      this.file = this.media.create(path);
    }
  }

  customBack() {
    // this.navCtrl.popToRoot({ animate: true, direction: 'back' });
    this.navCtrl.navigateRoot('/home', {
      animated: true,
      animationDirection: 'back',
    });
  }

  swipeEvent(ev) {
    console.log('swipeEvent: ', ev.directon);
    if (ev.direction === 4) {
      // RIGHT
      // this.navCtrl.push(HomePage);
      // this.navCtrl.popToRoot({ animate: true, direction: 'back' });
      this.navCtrl.navigateRoot('/home', {
        animated: true,
        animationDirection: 'back',
      });
    } else if (ev.direction === 2) {
      // LEFT
      // this.navCtrl.push(MetaphorPage, { id: this.selectedItem });
      this.navCtrl.navigateForward('/metaphor', {
        queryParams: {
          id: this.selectedItem,
        },
      });
    }
  }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { MeditationPage } from './meditation';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: MeditationPage,
      },
    ]),
  ],
  declarations: [MeditationPage],
})
export class MeditationPageModule {}

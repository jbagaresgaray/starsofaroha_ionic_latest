import { Component, ViewEncapsulation } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'page-soundtrack',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'soundtrack.html',
  styleUrls: ['soundtrack.scss'],
})
export class SoundtrackPage {
  constructor(private menuCtrl: MenuController) {}

  ionViewDidEnter() {
    console.log('ionViewDidEnter SoundtrackPage');
  }

  menuToggle() {
    this.menuCtrl.enable(true, 'main-menu');
    this.menuCtrl.toggle('main-menu');
  }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { SoundtrackPage } from './soundtrack';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: SoundtrackPage,
      },
    ]),
  ],
  declarations: [SoundtrackPage],
})
export class SoundtrackPageModule {}

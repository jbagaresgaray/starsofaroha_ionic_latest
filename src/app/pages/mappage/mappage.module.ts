import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { MappagePage } from './mappage';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: MappagePage,
      },
    ]),
  ],
  declarations: [MappagePage],
})
export class MappagePageModule {}

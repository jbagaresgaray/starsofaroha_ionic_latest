import {
  Component,
  ViewChild,
  ElementRef,
  NgZone,
  OnInit,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import {
  GoogleMaps,
  GoogleMapOptions,
  GoogleMapsMapTypeId,
  Marker,
  GoogleMapsAnimation,
  GoogleMapsEvent,
  HtmlInfoWindow,
} from '@ionic-native/google-maps/ngx';

import * as _ from 'lodash';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

declare const google: any;
declare const window: any;

@Component({
  selector: 'page-mappage',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'mappage.html',
  styleUrls: ['mappage.scss'],
})
export class MappagePage implements OnInit, OnDestroy {
  @ViewChild('map', { static: true }) mapElement: ElementRef;

  map: any;
  geoFencesArr: any[] = [];
  showMenu = false;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    private googleMaps: GoogleMaps,
    public zone: NgZone,
    public services: DataserviceProvider
  ) {}

  ionViewDidEnter() {
    this.showMenu = true;
    this.loadInteractiveWebMap();
  }

  ngOnInit() {
    window.angularComponentRef = { component: this, zone: this.zone };
  }

  ngOnDestroy() {
    window.angularComponentRef = null;
  }

  navigateMeditation(i) {
    console.log('navigateMeditation: ', i);
    this.navCtrl.navigateForward('/sliderpage', {
      queryParams: {
        id: i,
        slideTo: 0,
      },
    });
  }

  private loadInteractiveWebMap() {
    const generateMarkers = (geoFencesArr: any) => {
      const infowindow = new google.maps.InfoWindow({
        maxWidth: 230,
      });

      for (let i = 0; i < geoFencesArr.length; i++) {
        const row = geoFencesArr[i];

        const marker = new google.maps.Marker({
          title: row.name,
          icon: row.icon,
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: new google.maps.LatLng(row.latitude, row.longitude),
        });
        row.marker = marker;
        row.marker.addListener(
          'click',
          ((mapMarker: any, il: any) => {
            return function() {
              infowindow.setContent(
                '<h3>' +
                  row.name +
                  '</h3><p style="text-align:center;"><img src="./assets/imgs/' +
                  (il + 1) +
                  '.png"></p><p>' +
                  row.definetion +
                  '</p>' +
                  // tslint:disable-next-line:max-line-length
                  '<button id="infoButton" onclick="window.angularComponentRef.zone.run(() => {window.angularComponentRef.component.navigateMeditation(' +
                  (il + 1) +
                  ');})">Read More..</button>'
              );
              infowindow.open(this.map, mapMarker);
            };
          })(marker, i)
        );
      }
    };

    const loadWebMap = () => {
      // console.log('loadWebMap');
      if (typeof google === 'object' && typeof google.maps === 'object') {
        const mapOptions2: GoogleMapOptions = {
          center: new google.maps.LatLng(
            this.geoFencesArr[9].latitude,
            this.geoFencesArr[9].longitude
          ),
          zoom: 5,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          zoomControl: true,
          mapTypeControl: false,
          scaleControl: false,
          streetViewControl: false,
          rotateControl: false,
          fullscreenControl: false,
        };
        this.map = new google.maps.Map(
          this.mapElement.nativeElement,
          mapOptions2
        );

        generateMarkers(this.geoFencesArr);
      }
    };

    const loadNativeMap = () => {
      const mapOptions: GoogleMapOptions = {
        camera: {
          target: {
            lat: this.geoFencesArr[9].latitude,
            lng: this.geoFencesArr[9].longitude,
          },
          zoom: 5,
        },
        mapType: GoogleMapsMapTypeId.ROADMAP,
        controls: {
          zoom: true,
          compass: true,
          mapToolbar: true,
        },
      };

      this.map = GoogleMaps.create(this.mapElement, mapOptions);
      generateMarkers(this.geoFencesArr);
    };

    this.services
      .getAllPoints()
      .then((data: any) => {
        if (!_.isEmpty(data)) {
          const pointsArr: any[] = data.points;
          _.each(pointsArr, (row: any) => {
            const myString = row.mapTitle;
            myString.replace(/<\/?[^>]+(>|$)/g, '');
            this.geoFencesArr.push({
              latitude: parseFloat(row.location.lat), // center of geofence radius
              longitude: parseFloat(row.location.lng),
              radius: parseFloat(row.location.radius), // radius to edge of geofence in meters
              name: myString,
              definetion: row.mapDesc,
              icon: 'assets/icon/pin.png',
            });
          });
        }
      })
      .then(() => {
        this.platform.ready().then(() => {
          // if (this.platform.is('cordova')) {
          //   loadNativeMap();
          // } else {
          //   loadWebMap();
          // }
          loadWebMap();
        });
      });
  }
}

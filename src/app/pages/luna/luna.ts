import { Component, ViewEncapsulation } from '@angular/core';
import {
  NavController,
  Platform,
  LoadingController,
  MenuController,
} from '@ionic/angular';
import { Media, MediaObject, MEDIA_STATUS } from '@ionic-native/media/ngx';
import { File, IFile, FileEntry } from '@ionic-native/file/ngx';

import { constant } from '../../constants/constant';

declare let cordova: any;

@Component({
  selector: 'page-luna',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'luna.html',
  styleUrls: ['luna.scss'],
})
export class LunaPage {
  mediaObj: MediaObject;
  playing: boolean;
  stop: boolean;
  loading: any;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    private media: Media,
    public loadingCtrl: LoadingController,
    private menuCtrl: MenuController,
    public file: File
  ) {
    this.stop = true;
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter LunaPage');
    this.initData();
  }

  ionViewWillLeave() {
    if (this.platform.is('cordova')) {
      if (this.mediaObj) {
        this.mediaObj.stop();

        this.playing = false;
        this.stop = true;
      }
    }
  }

  private createMedia(newPath) {
    this.mediaObj = this.media.create(newPath);
    this.mediaObj.onStatusUpdate.subscribe(status => {
      console.log('Media status: ', status);
      if (status === MEDIA_STATUS.STARTING) {
        this.loading.dismiss();
      } else if (status === MEDIA_STATUS.RUNNING) {
        this.loading.dismiss();
      }
    });

    this.mediaObj.onSuccess.subscribe(() => {
      console.log('Media Action is successful');
    });

    this.mediaObj.onError.subscribe(error => {
      console.log('Media Error!', error);
      console.log('Media Error!', JSON.stringify(error));
      this.loading.dismiss();
    });
  }

  private initData() {
    let path: string;

    if (this.platform.is('cordova')) {
      this.platform.ready().then(() => {
        if (this.platform.is('ios')) {
          path =
            this.file.documentsDirectory +
            constant.mediaFolder +
            '/' +
            constant.packageFolder +
            '/Lunas_Lullaby.mp3';

          this.file.resolveLocalFilesystemUrl(path).then((entry: FileEntry) => {
            const newPath = entry.toInternalURL();
            this.createMedia(newPath);
          });
        } else if (this.platform.is('android')) {
          path =
            this.file.dataDirectory +
            constant.mediaFolder +
            '/' +
            constant.packageFolder +
            '/Lunas_Lullaby.mp3';
          console.log('Lunas_Lullaby: ', path);

          this.file.resolveLocalFilesystemUrl(path).then((entry: FileEntry) => {
            const newPath = entry.toInternalURL();
            console.log('newPath: ', newPath);
            this.createMedia(newPath);
          });
        }
      });
    }
  }

  async buttonPlayAudioEv() {
    this.loading = await this.loadingCtrl.create({
      showBackdrop: false,
    });
    if (this.platform.is('cordova')) {
      this.loading.present();
      this.mediaObj.play({
        numberOfLoops: 1,
        playAudioWhenScreenIsLocked: true,
      });
    } else {
      this.loading.dismiss();
    }
    this.playing = true;
    this.stop = false;
  }

  buttonPauseAudioEv() {
    if (this.platform.is('cordova')) {
      if (this.mediaObj) {
        this.mediaObj.pause();
      }
    }
    this.playing = false;
    this.stop = false;
  }

  buttonStopAudioEv() {
    if (this.platform.is('cordova')) {
      if (this.mediaObj) {
        this.mediaObj.stop();
      }
    }

    this.playing = false;
    this.stop = true;
  }

  menuToggle() {
    this.menuCtrl.enable(true, 'main-menu');
    this.menuCtrl.toggle('main-menu');
  }
}

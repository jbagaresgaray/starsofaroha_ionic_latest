import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { LunaPage } from './luna';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: LunaPage,
      },
    ]),
  ],
  declarations: [LunaPage],
})
export class LunaPageModule {}

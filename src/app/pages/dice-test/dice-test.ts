import { Component, ViewEncapsulation } from '@angular/core';
import { NavController, NavParams, Platform } from '@ionic/angular';
import * as $ from 'jquery';
import { DeviceMotion } from '@ionic-native/device-motion/ngx';

import { EventsService } from '../../providers/events.service';

@Component({
  selector: 'page-dice-test',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'dice-test.html',
})
export class DiceTestPage {
  subscription: any;
  previousAcceleration: any = {
    x: null,
    y: null,
    z: null,
  };
  shakeStarted = false;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    private deviceMotion: DeviceMotion,
    public events: EventsService
  ) {
    /* this.events.subscribe('shakeStop', () => {
            console.log('shakeStop');
            if (this.viewCtrl.name === 'DiceTestPage'){
                this.rollDice();
            }
        });
        console.log('viewCtrl: ', this.viewCtrl.name);*/
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter DiceTestPage');
  }

  rollDice() {
    const $die: any = $('.die'),
      sides = 20,
      initialSide = 1,
      transitionDuration = 500,
      animationDuration = 3000;

    const ctrl = this;
    let lastFace: any, timeoutId: any;

    const randomFace = () => {
      const face = Math.floor(Math.random() * sides) + initialSide;
      lastFace = face === lastFace ? randomFace() : face;
      return face;
    };

    const rollTo = (face: any) => {
      clearTimeout(timeoutId);

      // $('ul > li > a').removeClass('active')
      // $('[href=' + face + ']').addClass('active')
      $die.attr('data-face', face);

      setTimeout(() => {
        console.log('face: ', face);
        ctrl.navCtrl.navigateForward('/meditation', {
          queryParams: { id: face },
        });
      }, 600);
    };

    const reset = () => {
      $die.attr('data-face', null).removeClass('rolling');
    };

    $die.addClass('rolling');
    clearTimeout(timeoutId);

    timeoutId = setTimeout(() => {
      $die.removeClass('rolling');

      rollTo(randomFace());
    }, animationDuration);
  }
}

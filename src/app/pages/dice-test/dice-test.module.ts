import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { DiceTestPage } from './dice-test';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: DiceTestPage,
      },
    ]),
  ],
  declarations: [DiceTestPage],
})
export class DiceTestPageModule {}

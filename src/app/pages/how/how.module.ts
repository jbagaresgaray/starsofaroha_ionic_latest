import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { HowPage } from './how';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: HowPage,
      },
    ]),
  ],
  declarations: [HowPage],
})
export class HowPageModule {}

import { Component, ViewEncapsulation } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'page-how',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'how.html',
  styleUrls: ['how.scss'],
})
export class HowPage {
  constructor(private menutCtrl: MenuController) {}

  ionViewDidEnter() {
    console.log('ionViewDidEnter HowPage');
  }

  menutToggle() {
    this.menutCtrl.enable(true, 'main-menu');
    this.menutCtrl.toggle('main-menu');
  }
}

import { Component, ViewEncapsulation } from '@angular/core';
import {
  NavController,
  Platform,
  NavParams,
  MenuController,
} from '@ionic/angular';
import {
  Router,
  ActivatedRoute,
  RouterEvent,
  NavigationEnd,
} from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as $ from 'jquery';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import { EventsService } from '../../providers/events.service';

@Component({
  selector: 'page-home',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'home.html',
  styleUrls: ['home.scss'],
})
export class HomePage {
  view: any;
  isRolling = false;
  unsubscribe$ = new Subject<any>();

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    private menutCtrl: MenuController,
    public events: EventsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public service: DataserviceProvider
  ) {
    this.router.events
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          console.log('this.activatedRoute: ', this.activatedRoute);

          if (this.activatedRoute.root) {
            this.activatedRoute.root.firstChild.data
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe((params: any) => {
                console.log('params: ', params);
                if (params && params.navigate) {
                  const navigate = params.navigate;
                  if (navigate === 'navigate') {
                    setTimeout(() => {
                      console.log('navigate');
                      this.callToRoll();
                    }, 1000);
                  }
                } else if (params && params.name) {
                  this.view = params;
                  this.service.activePage = params.name;
                }
              });
          }
        }
      });

    this.events.subscribe('callToRoll', () => {
      this.callToRoll();
    });
  }

  ionViewWillEnter() {
    $('.dicecontent').hide();
  }

  ionViewWillLeave() {
    this.events.unsubscribe('notification:click');

    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  callToRoll() {
    const ctrl = this;
    $('.dicecontent').fadeIn(600, () => {
      $('.dicecontent').show();
      setTimeout(() => {
        ctrl.rollDice();
      }, 300);
    });

    // ctrl.navCtrl.navigateForward('/sliderpage', {
    //   queryParams: {
    //     id: 20,
    //     slideTo: 0,
    //   },
    // });
  }

  menutToggle() {
    this.menutCtrl.enable(true, 'main-menu');
    this.menutCtrl.toggle('main-menu');
  }

  private rollDice() {
    console.log('private rollDice');

    const $die: any = $('.die'),
      sides = 20,
      initialSide = 1,
      transitionDuration = 500,
      animationDuration = 3000;
    const ctrl = this;

    let lastFace: any, timeoutId: any;

    console.log('this.isRolling: ', this.isRolling);
    if (this.isRolling === true) {
      this.isRolling = false;
      return false;
    }

    this.isRolling = true;

    const randomFace = () => {
      const face = Math.floor(Math.random() * sides) + initialSide;
      lastFace = face === lastFace ? randomFace() : face;
      console.log('randomFace: ', face);
      return face;
    };

    const rollTo = (face: any) => {
      console.log('rollTo: ', face);
      clearTimeout(timeoutId);

      // $('ul > li > a').removeClass('active')
      // $('[href=' + face + ']').addClass('active')
      $die.attr('data-face', face);
      setTimeout(() => {
        console.log('face: ', face);
        ctrl.navCtrl.navigateForward('/sliderpage', {
          queryParams: {
            id: face,
            slideTo: 0,
          },
        });
      }, 1000);
    };

    const reset = () => {
      $die.attr('data-face', null).removeClass('rolling');
    };

    $die.addClass('rolling');
    clearTimeout(timeoutId);

    timeoutId = setTimeout(() => {
      $die.removeClass('rolling');
      rollTo(randomFace());
    }, animationDuration);
    this.isRolling = false;
  }
}
